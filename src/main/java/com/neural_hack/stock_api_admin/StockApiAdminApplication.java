package com.neural_hack.stock_api_admin;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAdminServer
@SpringBootApplication
public class StockApiAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockApiAdminApplication.class, args);
    }

}
